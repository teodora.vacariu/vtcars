### VTCARS PROJECT
## About the project
#### https://gitlab.upt.ro/teodora.vacariu/vtcars
VTCars is an online marketaplace for buying and selling new or second-hand cars. 
Frontend is made with SAP-UI5 Javascript Framework, whereas Backend is made with node.js and MongoDB.
## Prerequisites
Install

- node.js
- mongoDB
- ui5@latest

## Installation
After cloning the repository, install the required packages using 
```bash
npm install
```

## Running the project
### Backend
In order to run the backend part of the project, follow these steps:
```bash
cd VTCars_BE/app
```
```bash
nodemon server.js
```

### Frontend
In order to run the frontend part of the project, follow these steps:
```bash
cd VTCars_FE
```
```bash
npm start
```
