import mongoose from "mongoose";
const brandsSchema = mongoose.Schema({
  brandName: {
    type: String,
    required: true,
  },
  models: [
    {
      type: String,
    },
  ],
});

const BrandsModel = mongoose.model("BrandsModel", brandsSchema);
export default BrandsModel;
