import mongoose, { mongo } from "mongoose";
const carAdSchema = mongoose.Schema(
  {
    bodyStyle: {
      type: String,
      required: true,
    },
    brand: {
      type: String,
      required: true,
    },
    model: {
      type: String,
      required: true,
    },
    year: {
      type: Number,
      required: true,
    },
    price: {
      type: Number,
      required: true,
    },
    fuelType: {
      type: String,
      required: true,
    },
    engineType: {
      type: String,
      required: true,
    },
    capacity: {
      type: Number,
      required: true,
    },
    mileage: {
      type: Number,
      required: true,
    },
    gearbox: {
      type: String,
      required: true,
    },
    drivetrain: {
      type: String,
      required: true,
    },
    isVerified: {
      type: Boolean,
      required: true,
    },
    title: {
      type: String,
      required: true,
    },
    documents: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "DocumentModel",
      },
    ],
    description: {
      type: String,
      required: true,
    },
    owner: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "UserModel",
    },

    images: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "images.files",
      },
    ],

    comments: [
      {
        user: {
        type: String
      },
        commentText: {
          type: String
        },
      }
    ]
  },
  { timestamps: true }
);

const CarAdModel = mongoose.model("CarAdModel", carAdSchema);

export default CarAdModel;