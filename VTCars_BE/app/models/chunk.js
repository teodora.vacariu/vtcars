import mongoose from "mongoose";

const chunkSchema = mongoose.Schema(
  {
    files_id: {
      type: Number,
      required: true,
    },

    chunkSize: {
      type: Number,
      required: true,
    },

    uploadDate: {
      type: Date,
      required: true,
    },

    filename: {
      type: String,
      required: true,
    },

    contentType: {
      type: String,
      required: true,
    },
  },
  { collection: "images.chunks" },
  { timestamps: true }
);

const ImageModel = mongoose.model("ImageModel", imageSchema);

export default ImageModel;
