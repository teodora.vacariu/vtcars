import mongoose from "mongoose";

const documentSchema = mongoose.Schema(
  {
    length: {
      type: Number,
      required: true,
    },

    chunkSize: {
      type: Number,
      required: true,
    },

    uploadDate: {
      type: Date,
      required: true,
    },

    filename: {
      type: String,
      required: true,
    },

    contentType: {
      type: String,
      required: true,
    },
  },
  { collection: "documents.files" },
  { timestamps: true }
);

const DocumentModel = mongoose.model("DocumentModel", documentSchema);

export default DocumentModel;
