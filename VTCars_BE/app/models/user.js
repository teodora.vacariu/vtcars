import mongoose from "mongoose";
const userSchema = mongoose.Schema(
  {
    firstName: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
    },
    city: {
      type: String,
      required: true,
    },
    county: {
      type: String,
      required: true,
    },
    phoneNumber: {
      type: String,
      required: true,
    },
    favourites: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "CarAdModel",
      },
    ],
    listedCars: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "CarAdModel",
      },
    ],
    ratingsSum: {
      type: Number
    },

    nrUsersRated: {
      type: Number
    },
    ratingMean:{
      type: Number
    },
    images: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "images.files",
      },
    ],
  },
  { timestamps: true }
);

const UserModel = mongoose.model("UserModel", userSchema);
export default UserModel;
