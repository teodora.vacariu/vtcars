import BrandsModel from "../models/brands.js";
import jwt from "jsonwebtoken";

export async function addBrand(req, res, next) {
  try {
    const auth = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);

    if (auth) {
      const brand = new BrandsModel({
        brandName: req.body.brandName,
        models: req.body.models,
      });

      const querry = await brand.save();

      if (querry) {
        res.status(201).json(querry);
      }
    }
  } catch (err) {
    next(err);
  }
}

export async function getBrandModel(req, res, next) {
  try {
    const auth = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);

    if (auth) {
      const data = await BrandsModel.find();

      if (data) {
        res.status(200).json(data);
      } else {
        res.status(404).end();
      }
    } else {
      res.status(401).end();
    }
  } catch (err) {
    next(err);
  }
}
