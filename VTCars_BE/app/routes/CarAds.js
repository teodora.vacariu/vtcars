import CarAdModel from "../models/carAd.js";
import UserModel from "../models/user.js";
import jwt from "jsonwebtoken";
import mongoose from "mongoose";
import ImageModel from "../models/image.js"
import DocumentModel from "../models/document.js"

const imgGFS = new mongoose.mongo.GridFSBucket(mongoose.connection, {
  bucketName: "images",
});

const docGFS = new mongoose.mongo.GridFSBucket(mongoose.connection, {
  bucketName: "documents",
});

function buildQuerry(obj) {
  // Get an array of obj's keys
  const keys = Object.keys(obj);

  const querry = {};

  for (let i = 0; i < keys.length; i++) {
    const key = keys[i];
    const val = obj[key];

    //If val is not empty string and not whitespaces, it's valid
    if (val !== "" && /\S/.test(val)) {
      querry[key] = val;
    }
  }

  // mileage
  if (querry.hasOwnProperty("maxMileage")) {
    querry.mileage = { $lte: querry.maxMileage };
    delete querry.maxMileage;
  }

  // year
  if (querry.hasOwnProperty("yearFrom") && querry.hasOwnProperty("yearTo")) {
    querry.year = { $gte: querry.yearFrom, $lte: querry.yearTo };
    delete querry.yearTo;
    delete querry.yearFrom;
  } else if (querry.hasOwnProperty("yearFrom")) {
    querry.year = { $gte: querry.yearFrom };
    delete querry.yearFrom;
  } else if (querry.hasOwnProperty("yearTo")) {
    querry.year = { $lte: querry.yearTo };
    delete querry.yearTo;
  }

  // price
  if (querry.hasOwnProperty("priceFrom") && querry.hasOwnProperty("priceTo")) {
    querry.price = { $gte: querry.priceFrom, $lte: querry.priceTo };
    delete querry.priceTo;
    delete querry.priceFrom;
  } else if (querry.hasOwnProperty("priceFrom")) {
    querry.price = { $gte: querry.priceFrom };
    delete querry.priceFrom;
  } else if (querry.hasOwnProperty("priceTo")) {
    querry.price = { $lte: querry.priceTo };
    delete querry.priceTo;
  }

  // capacity
  if (
    querry.hasOwnProperty("capacityFrom") &&
    querry.hasOwnProperty("capacityTo")
  ) {
    querry.capacity = { $gte: querry.capacityFrom, $lte: querry.capacityTo };
    delete querry.capacityTo;
    delete querry.capacityFrom;
  } else if (querry.hasOwnProperty("capacityFrom")) {
    querry.capacity = { $gte: querry.capacityFrom };
    delete querry.capacityFrom;
  } else if (querry.hasOwnProperty("capacityTo")) {
    querry.capacity = { $lte: querry.capacityTo };
    delete querry.capacityTo;
  }

  if (querry.hasOwnProperty("isVerified")) {
    if (querry.isVerified === "Da") {
      querry.isVerified = true;
    } else if (querry.isVerified === "Nu") {
      querry.isVerified = false;
    }
  }

  return querry;
}

export async function cars(req, res, next) {
  try {
    const auth = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);
    if (auth) {
      const filter = buildQuerry(req.body);
      const data = await CarAdModel.find(filter);
      if (data.length > 0) {
        res.status(200).json(data);
      } else {
        // .end() is to make sure the response is not left hanging, alternatively use .json({error: "Not Found"})
        res.status(404).end();
      }
    } else {
      res.status(401).end();
    }
  } catch (err) {
    next(err);
  }
}

export async function car(req, res, next) {
  try {
    const auth = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);

    if (auth) {
      const user = await UserModel.findById(req.body.owner);

      if (user) {
        const newCar = new CarAdModel({
          bodyStyle: req.body.bodyStyle,
          brand: req.body.brand,
          model: req.body.model,
          year: req.body.year,
          price: req.body.price,
          fuelType: req.body.fuelType,
          engineType: req.body.engineType,
          capacity: req.body.capacity,
          gearbox: req.body.gearbox,
          drivetrain: req.body.drivetrain,
          isVerified: req.body.isVerified,
          description: req.body.description,
          mileage: req.body.mileage,
          owner: user._id,
          images: req.body.images,
          documents: req.body.documents,
          title: req.body.title,
        });

        const response = await newCar.save();

        if (response) {
          user.listedCars.push(newCar._id);
          const userResponse = user.save();

          if (userResponse) {
            res.status(201).json(response);
          }
        }
      } else {
        res.status(400).json({ error: `User ${req.body.id} does not exist` });
      }
    } else {
      res.status(401).end();
    }
  } catch (err) {
    next(err);
  }
}
export async function addComment(req, res, next) {
  try{
    const auth=jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);

    if(auth){
      const user = await UserModel.findById(req.cookies.id); 
   
      if(user) {
        const comment = {user : user.firstName, commentText: String(req.body.comment)}; 
        const selectedCar = await CarAdModel.findById(req.body.selectedCar);

        if(selectedCar){
          selectedCar.comments.push(comment);
          const response = selectedCar.save();

          if(response){
            res.status(201).json(selectedCar.comments);
          }
        }
      }
    } else {
      res.status(401).end();
    }
  } catch (err) {
    next(err);
  }
}

export async function brand(req, res, next) {
  try {
    const auth = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);

    if (auth) {
      const data = await CarAdModel.find({ brand: req.params.brand });
      if (data) {
        res.status(200).json(data);
      } else {
        res.status(404).end();
      }
    }
  } catch (err) {
    next(err);
  }
}

export async function bodyStyle(req, res, next) {
  try {
    const auth = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);

    if (auth) {
      const data = await CarAdModel.find({ bodyStyle: req.params.body });

      if (data) {
        res.status(200).json(data);
      } else {
        res.status(404).end();
      }
    } else {
      res.status(401).end();
    }
  } catch (err) {
    next(err);
  }
}

export async function findCarById(req, res, next) {
  try {
    const auth = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);

    if (auth) {
      const data = await CarAdModel.findById(req.params.id);

      if (data) {
        res.status(200).json(data);
      } else {
        res.status(404).end();
      }
    } else {
      res.status(401).end();
    }
  } catch (err) {
    next(err);
  }
}

export async function addToFavourites(req, res, next) {
  try {
    const auth = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);

    if (auth) {
      const data = await UserModel.findById(req.cookies.id);

      if (data) {
        if (data.favourites.indexOf(req.params.id) === -1) {
          data.favourites.push(req.params.id);
          const response = await data.save();
          if (response) {
            res.status(201).json(data);
          }
        } else {
          res
            .status(409)
            .json({
              error: `Car ${req.params.id} already exists in user's favourites`,
            });
        }
      } else {
        res.status(404).end();
      }
    } else {
      res.status(401).end();
    }
  } catch (err) {
    next(err);
  }
}

export async function addToHistory(req, res, next) {
  try {
    const auth = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);

    if (auth) {
      const data = await UserModel.updateOne(
        { _id: req.params.userId },
        { $push: { listedCars: req.body._id } }
      );
      if (data) {
        res.status(201).json(data);
      } else {
        res.status(404).end();
      }
    } else {
      res.status(401).end();
    }
  } catch (err) {
    next(err);
  }
}

async function deleteImg(id) {
  imgGFS.delete(id);
}

async function deleteDoc(id) {
  docGFS.delete(id);
}

export async function deleteCar(req, res, next) {
  try {
    const auth = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);

    if (auth) {
      // Remove car from user's listedCars
      const user = await UserModel.findById(req.cookies.id);

      const listedArray = user.listedCars.filter((carId) => {
        const ID = JSON.stringify(carId).split(/[\"()]/);
        return ID[ID.length - 2] !== req.params.id;
      });

      user.listedCars = listedArray;

      // Remove car from users's favourites

      const favouritesArray = user.favourites.filter((carId) => {
        const ID = JSON.stringify(carId).split(/[\"()]/);
        return ID[ID.length - 2] !== req.params.id;
      });

      user.favourites = favouritesArray;
      user.save();

      const car = await CarAdModel.findById(req.params.id);

      if (car) {
        // Delete the images associated with a car
        car.images.forEach(async (imageID) => {
          const image = await ImageModel.findById(imageID);
          if(image) {
            imgGFS.delete(imageID);
          } else {
            return;
          }
        });

        // Delete the documents associated with a car

        car.documents.forEach(async (documentID) => {
           const document = await DocumentModel.findById(documentID);
           if(document) {
              docGFS.delete(documentID);
           } else {
            return;
           }
        });

        // Now, the only thing left to do is delete the car document

        const response = await car.remove();

        if (response) {
          res.status(204).end();
        }
      }
    } else {
      res.status(401).end();
    }
  } catch (err) {
    next(err);
  }
}

export async function removeFavourites(req, res, next) {
  try {
    const auth = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);
    if (auth) {
      const user = await UserModel.findById(req.cookies.id);
      const favouritesArray = user.favourites.filter((carId) => {
        const ID = JSON.stringify(carId).split(/[\"()]/);
        return ID[ID.length - 2] !== req.params.id;
      });
      user.favourites = favouritesArray;
      const response = await user.save();

      if (response) {
        res.status(200).json(response);
      }
    } else {
      res.status(401).end();
    }
  } catch (err) {
    next(err);
  }
}