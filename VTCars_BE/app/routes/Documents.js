import mongoose from "mongoose";
import dotenv from "dotenv";
import jwt from "jsonwebtoken";
import DocumentModel from "../models/document.js";

dotenv.config();

const docGFS = new mongoose.mongo.GridFSBucket(mongoose.connection, {
  bucketName: "documents",
});


export async function doc(req, res, next) {
  try {
    const auth = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);

    if (auth) {
      const data = await DocumentModel.findById(req.params.id);

      if (data) {
        docGFS.openDownloadStreamByName(data.filename).pipe(res);
      } else {
        res.status(404).end();
      }
    } else {
      res.status(401).end();
    }
  } catch (err) {
    next(err);
  }
}

export async function uploadDocs(req, res, next) {
  try {
    if (req.files) {
      res.status(201).json(req.files);
    }
  } catch (err) {
    next(err);
  }
}

export async function deleteDocument(req, res, next) {
    try {
      const doc = await DocumentModel.findById(req.params.id);
      
      if(doc){
        docGFS.delete(doc._id, (err) => {
          if(err) {
            next(err);
          } else {
            res.status(204).end();
          }
        })
      } else {
        res.status(404).json({error: `Document ${req.params.id} not found`});
      }
    } catch (err) {
      next(err);
    }
}
