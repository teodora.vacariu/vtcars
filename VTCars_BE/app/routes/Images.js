import ImageModel from "../models/image.js";
import mongoose from "mongoose";
import dotenv from "dotenv";
import jwt from "jsonwebtoken";

dotenv.config();

if (mongoose.connection.db === undefined) {
  await mongoose.connect(process.env.MONGO_URI);
}

const imgGFS = new mongoose.mongo.GridFSBucket(mongoose.connection, {
  bucketName: "images",
});

async function getImage(id) {
  const image = await ImageModel.findById(id);
  return image;
}

export async function image(req, res, next) {
  try {
    const auth = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);

    if (auth) {
      const image = await getImage(req.params.imgID);

      if (image) {
        imgGFS.openDownloadStreamByName(image.filename).pipe(res);
      } else {
        res.status(404).end();
      }
    } else {
      res.status(401).end();
    }
  } catch (err) {
    next(err);
  }
}

export async function postImages(req, res, next) {
  try {
    if (req.files) {
      res.status(201).json(req.files);
    }
  } catch (err) {
    next(err);
  }
}

export async function deleteImage(req, res, next) {
  try {

    const img = await ImageModel.findById(req.params.imgID);
    if(img){
      imgGFS.delete(img._id, (err) => {
        if(err) {
          next(err);
        } else {
          res.status(204).end();
        }
      })
    } else {
      res.status(404).json({error: `Image ${req.params.imgID} not found`});
    }
  } catch (err) {
    next(err);
  }
}
