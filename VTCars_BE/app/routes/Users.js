import jwt from "jsonwebtoken";
import UserModel from "../models/user.js";
import mongoose from "mongoose";
import bcrypt from "bcryptjs";
import CarAdModel from "../models/carAd.js";
import CryptoJS from "crypto-js";
import ImageModel from "../models/image.js"
import nodemailer from "nodemailer"

// const imgGFS = new mongoose.mongo.GridFSBucket(mongoose.connection, {
//   bucketName: "images",
// });

function validatePhone(phone) {
  const regex = /^\+?\d{10}$/;
  return regex.test(phone);
}
function validateEmail(email) {
  const regex = /^\w+[\w-+\.]*\@\w+([-\.]\w+)*\.[a-zA-Z]{2,}$/;
  return regex.test(email);
}
function validateUser(user) {
  const keys = Object.keys(user);
  for (let i = 0; i < keys.length; i++) {
    if (typeof user[keys[i]] === "string") {
      if (!/\S/.test(user[keys[i]]) || user[keys[i]] === "") {
        return false;
      }
    }
  }
  return true;
}
export async function register(req, res, next) {
  try {
    req.body.password = CryptoJS.AES.decrypt(
      req.body.password,
      process.env.ENCRYPTION_SECRET
    ).toString(CryptoJS.enc.Utf8);

    const userData = {
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      password: req.body.password,
      confirm: req.body.confirm_password,
      city: req.body.city,
      county: req.body.county,
      phoneNumber: req.body.phoneNumber,
      favourites: [],
      listedCars: [],
      ratingsSum: 0,
      nrUsersRated: 0,
      ratingMean: 0,
      //images: []
    };

    if (validatePhone(userData.phoneNumber) === false) {
      res.status(400).json({ error: "Invalid phone number" });
      return;
    }
    if (validateEmail(userData.email) === false) {
      res.status(400).json({ error: "Invalid email" });
      return;
    }
    // if (validateUser(userData) === false) {
    //   res.status(400).json({ error: "Some fields are not filled in" });
    //   return;
    // }
    if (userData.confirm !== userData.password) {
      res.status(400).json({ error: "Password fields don't match" });
      return;
    }

    delete userData.confirm;

    const user = await UserModel.findOne({ email: userData.email });
    if (!user) {
      const hash = bcrypt.hashSync(req.body.password, 10);

      if (hash) {
        userData.password = hash;

        const response = await UserModel.create(userData);

        if (response) {
          res.status(201).json({ email: response.email, _id: response._id });
        }
      }
    } else {
      res
        .status(409)
        .json({ error: `Un cont cu mailul ${userData.email} exista deja` });
    }
  } catch (err) {
    next(err);
  }

}

export async function login(req, res, next) {
  try {
    req.body.password = CryptoJS.AES.decrypt(
      req.body.password,
      process.env.ENCRYPTION_SECRET
    ).toString(CryptoJS.enc.Utf8);

    const user = await UserModel.findOne({ email: req.body.email });

    if (user) {
      if (bcrypt.compareSync(req.body.password, user.password)) {
        const token = jwt.sign(user.email, process.env.ACCESS_TOKEN_SECRET, {});

        res.cookie("token", token, {sameSite: "None", secure: true})
        res.cookie("id", user.id, {sameSite: "None", secure: true})
        res.status(200).json({
          _id: user.id,
          token: token,
        });
      } else {
        res.status(401).end();
      }
    } else {
      res.status(401).end();
    }
  } catch (err) {
    next(err);
  }
}

export async function auth(req, res, next) {
  try {
    // First, check if the user with the provided id exists
    const data = await UserModel.findById(req.cookies.id);
   
    if (data) {
      // Verify the provided token
      const decoded = jwt.verify(
        req.cookies.token,
        process.env.ACCESS_TOKEN_SECRET
      );
     
      if (decoded) {
        res.status(200).json(decoded);
      } else {
        res.status(401).end();
      }
    } else {
      res
        .status(404)
        .json({ message: `User with id ${req.body._id} not found` });
    }
  } catch (err) {
    next(err);
  }
}

export async function users(req, res, next) {
  try {
    const auth = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);
    if (auth) {
      // destructure page and limit and set default values
      let { page = 1, limit = 2 } = req.query;

      // execute query with page and limit values
      const users = await UserModel.find()
        .limit(limit * 1)
        .skip((page - 1) * limit)
        .exec();

      // get total documents in the users collection
      let count = await UserModel.countDocuments();

      // return response with users, total pages, and current page
      if (users) {
        res.status(200).json({
          users,
          totalPages: Math.ceil(count / limit),
          currentPage: page,
        });
      } else {
        res.status(404).end();
      }
    }
  } catch (err) {
    next(err);
  }
}

export function user(req, res, next) {
  
  try{
    const auth = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);
  if (auth) {
    const id = req.params.id;
    UserModel.findById(id)
      .then((result) => {
        if (result) {
          res.status(200).send(result);
        } else {
          res.status(404).end();
        }
      })
      .catch((err) => {
        next(err);
      });
  } else {
    res.status(401).end();
  }
} catch(err){
  next(err)
}
}
// export async function changeProfilePic(req, res, next){
//   const auth = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);
//   if (auth) {
//     const id = req.cookies.id;
//     const user = await UserModel.findById(id);
//     if(user){
//       user.images = req.images;
//       const response = user.save();

//       if(response){
//         res.status(201).json(user);
//       }
//     }
//   } else {
//     res.status(401).end();
//   }
// }

export function update(req, res, next) {
  const auth = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);
  if (auth) {
    const id = req.cookies.id;

    if (validatePhone(req.body.phoneNumber) === false) {
      res.status(400).json({ error: "Invalid phone number" });
      return;
    }
    if (validateUser(req.body) === false) {
      res.status(400).json({ error: "Some fields are not filled in" });
      return;
    }
    if (typeof req.body.city != "string") {
      next(new TypeError("Incorrect type for city"));
    }
    if (validateEmail(req.body.email) === false) {
      res.status(400).json({ error: "Invalid email" });
      return;
    }
    if (typeof req.body.firstName != "string") {
      next(new TypeError("Incorrect type of first name"));
    }
    if (typeof req.body.lastName != "string") {
      next(new TypeError("Incorrect type of last name"));
    }

    UserModel.findByIdAndUpdate(id, req.body)
      .then((result) => {
        if (result) {
          res.status(201).json(result);
        }
      })
      .catch((err) => {
        next(err);
      });
  } else {
    res.status(401).end();
  }
}

export async function change_password(req, res, next) {
 
  try {
    const auth = jwt.verify(req.cookies.emailToken, process.env.ACCESS_TOKEN_SECRET);
    if (auth) {

      req.body.new_password = CryptoJS.AES.decrypt(
        req.body.new_password,
        process.env.ENCRYPTION_SECRET
      ).toString(CryptoJS.enc.Utf8);

      const user = await UserModel.findById(req.cookies.id);
    
      if (bcrypt.compareSync(req.body.new_password, user.password)) {

        res.status(409).json({ error: "New password must be different from current password" });
        return
      }
      if (req.body.new_password !== req.body.confirm_new_password) {
        res.status(400).json({ error: "Passwords don't match!" });
        return;
      } else {
        user.password = bcrypt.hashSync(
          req.body.new_password,
          10
        );
        delete req.body.new_password;
        delete req.body.confirm_new_password;
        
        const response = await user.save();
         
        if (response) {
          res.status(201).json({ message: "Password updated! You may close the tab" });
        }
      }
    }
  } catch (err) {
    next(err);
  }
}

export function getFavouritesFromUser(req, res, next) {
  const auth = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);
  if (auth) {
    const id = req.cookies.id;
    UserModel.findById(id)
      .then(async (data) => {
        if (data) {
          for (let index = 0; index < data.favourites.length; index++) {
            const car = await CarAdModel.findById(data.favourites[index]);
            if (!car) {
              data.favourites.splice(index, 1);
              index--;
            }
          }
          await data.save();
          data = await data.populate("favourites");
          res.status(200).json(data.favourites);
        }
      })
      .catch((err) => {
        next(err);
      });
  }
}

export async function sendEmail(req, res, next) {
  try {
    const auth = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);
    if (auth) {
      const id = req.cookies.id;
      const user = await UserModel.findById(req.cookies.id);
      const smtpTrans = nodemailer.createTransport({
        service: "gmail",
        auth: {
          user: "autohype.contact@gmail.com",
          pass: process.env.GOOGLE_MAIL,
        }
      });

      const mailOptions = {
        from: "autohype.contact@gmail.com",
        to: user.email,
        subject: "Password change request",
        html: "<h1>Click <a href=http://localhost:8080/#/ChangePassword>here</a> to change your password.</h1>"
      }

      smtpTrans.sendMail(mailOptions, async function (error, response) {
        if (error) {
          next(error);
        } else {
          const token = jwt.sign({id: user.id}, process.env.ACCESS_TOKEN_SECRET, {expiresIn: 600});
          res.cookie("emailToken", token, {sameSite: "None", secure: true});
          res.status(200).json({"emailToken": token});
        }
      });
    }
  } catch (error) {
    next(error);
  }
}

export async function rate(req, res, next) { 
  try{
    const auth = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);
    if(auth) {
      const stars = req.body.stars;
      const sellerId = req.body.sellerID;
      const ratedUser = await UserModel.findById(sellerId); // sellerul rate uit 
      ratedUser.nrUsersRated = ratedUser.nrUsersRated + 1;
      ratedUser.ratingsSum = ratedUser.ratingsSum + Number(stars); //suma ratingurilor
      ratedUser.ratingMean = Number(ratedUser.ratingsSum/ ratedUser.nrUsersRated).toFixed(1);
      const response = await ratedUser.save();
      if(response){
        res.status(200).json(ratedUser);
      }
    }
  }
  catch(err) {
    next(err)
  }
}