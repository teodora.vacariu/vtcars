import express from "express";
const router = express.Router();
import { GridFsStorage } from "multer-gridfs-storage";
import {
  register,
  login,
  auth,
  users,
  user,
  update,
  getFavouritesFromUser,
  change_password,
  sendEmail,
  rate,
  //changeProfilePic
} from "./Users.js";
import { addBrand, getBrandModel } from "./Brands.js";
import {
  cars,
  car,
  brand,
  bodyStyle,
  addToFavourites,
  findCarById,
  deleteCar,
  addToHistory,
  removeFavourites,
  addComment
} from "./CarAds.js";
import { image, postImages, deleteImage } from "./Images.js";
import { doc, uploadDocs, deleteDocument } from "./Documents.js";
import dotenv from "dotenv";
import multer from "multer";
import jwt from "jsonwebtoken";

dotenv.config();

export const imgStorage = new GridFsStorage({
  url: process.env.MONGO_URI,
  file: (req, file) => {
    return {
      filename: file.originalname,
      bucketName: "images",
    };
  },
});

export const docStorage = new GridFsStorage({
  url: process.env.MONGO_URI,
  file: (req, file) => {
    return {
      filename: file.originalname,
      bucketName: "documents",
    };
  },
});

const imgUpload = await multer({
  storage: imgStorage,
  //Validate file MIME types
  fileFilter: function (req, file, cb) {
    if (
      file.mimetype === "image/jpeg" ||
      file.mimetype === "image/png" ||
      file.mimetype === "image/svg+xml" ||
      file.mimetype === "image/webp"
    ) {
      return cb(null, true);
    } else {
      return cb(
        new TypeError(`MIME type '${file.mimetype}' is not acceptable.`)
      );
    }
  },

  // Limit the ammount of uploaded images to at most 5 per post
  limits: {
    files: 4,
  },
});

const docUpload = await multer({
  storage: docStorage,

  fileFilter: function (req, file, cb) {
    if (
      file.mimetype === "application/msword" ||
      file.mimetype ===
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document" ||
      file.mimetype === "application/pdf"
    ) {
      return cb(null, true);
    } else {
      return cb(
        new TypeError(`MIME type '${file.mimetype}' is not acceptable`)
      );
    }
  },

  limits: {
    files: 1,
    fileSize: 5000000, // given in bytes, equivalent to 5 MB
  },
});

// user routes
//router.route("/changeProfilePic").post(changeProfilePic)
router.route("/register").post(register);
router.route("/login").post(login);
router.route("/auth").post(auth);
router.route("/users").get(users);
router.route("/user/:id").get(user); //get user by id
router.route("/user/update").put(update);
router.route("/userr/favourites").get(getFavouritesFromUser);
router.route("/user/changePassword").put(change_password);
router.route("/sendEmail").post(sendEmail);
router.route("/rate").post(rate);
// img routes
router.route("/image/:imgID").get(image);
router.route("/images").post(
  function (req, res, next) {
    try {
      const auth = jwt.verify(
        req.cookies.token,
        process.env.ACCESS_TOKEN_SECRET
      );

      if (auth) {
        // If the request is authorized, then the next function (imgUpload) is called
        next();
      } else {
        res.status(401).end();
      }
    } catch (err) {
      next(err);
    }
  },
  imgUpload.array("files[]"),
  postImages
);
router.route("/image/:imgID").delete(deleteImage);

// doc routes
router.route("/documents").post(
  function (req, res, next) {
    try {
      const auth = jwt.verify(
        req.cookies.token,
        process.env.ACCESS_TOKEN_SECRET
      );

      if (auth) {
        next();
      } else {
        res.status(401).end();
      }
    } catch (err) {
      next(err);
    }
  },
  docUpload.array("docs[]"),
  uploadDocs
);
router.route("/document/:id").get(doc);
router.route("/document/:id").delete(deleteDocument);

// brand routes
router.route("/addBrand").post(addBrand);
router.route("/getBrandModel").get(getBrandModel);

// car routes
router.route("/cars").post(cars);
router.route("/car").post(car);
router.route("/car/body/:body").get(bodyStyle);
router.route("/car/brand/:brand").get(brand);
router.route("/car/:id").get(findCarById);
router.route("/add-to-favourites/:id").post(addToFavourites);
router.route("/add-to-listedCars/:userId").post(addToHistory);
router.route("/car/:id").delete(deleteCar);
router.route("/fav/:id").delete(removeFavourites);
router.route("/carAd/comment").post(addComment);

export default router;
