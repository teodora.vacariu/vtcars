import express from "express";
import mongoose, { mongo } from "mongoose";
import bodyParser from "body-parser";
import dotenv from "dotenv";
import cors from "cors";
import routes from "./routes/index.js";
import cookieParser from "cookie-parser";
import jwt from "jsonwebtoken";
import { MulterError } from "multer";

const app = express();
const port = process.env.PORT;

dotenv.config();
app.use(cookieParser());
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(
  // localhost and BTP URIs
  cors({
    origin: ["http://localhost:8080"],
    credentials: true,
  })
);
//connect to mongodb
const mon = await mongoose.connect(process.env.MONGO_URI);

if (mon) {
  console.log("Successfully connected to MongoDB");
}

app.use("/", routes);

// error handler
app.use((err, req, res, next) => {
  console.log(err);
  switch (err.constructor) {
    case jwt.JsonWebTokenError:
    case jwt.TokenExpiredError:
      res.status(401).end();
      break;
    case mongoose.Error.CastError:
    case mongoose.Error.ValidationError:
    case SyntaxError:
    case TypeError:
    case MulterError:
    case RangeError:
    case mongo.MongoRuntimeError:
      res.status(400).json({ name: err.name, message: err.message });
      break;
    default:
      res.status(500).json({ name: err.name, message: err.message });
  }
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});

export default mon;
