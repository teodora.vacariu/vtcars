sap.ui.define(
  ["sap/ui/core/UIComponent", "sap/ui/model/json/JSONModel"],
  (UIComponent, JSONModel) => {
    return UIComponent.extend("VTCARS_PROJECT.Component", {
      metadata: {
        manifest: "json",
      },

      /**
       * The component is initialized by UI5 automatically during the
       * startup of the app and calls the init method once.
       * @public
       * @override
       */
      init() {
        let filteredBy = new JSONModel();
        this.setModel(filteredBy, "filteredBy");

        let selectedCar = new JSONModel();
        this.setModel(selectedCar, "selectedCar");
        // Initialize UI component
        // eslint-disable-next-line prefer-rest-params
        UIComponent.prototype.init.apply(this, arguments);

        this.getRouter().initialize();
      },
    });
  }
);
