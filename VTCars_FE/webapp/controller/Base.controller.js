sap.ui.define(
  [
    "sap/ui/core/mvc/Controller",
    "sap/ui/core/Fragment",
    "jquery.sap.global",
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/BusyIndicator",
    "VTCARS_PROJECT/utils/AjaxClient",
    "VTCARS_PROJECT/utils/Credentials",
  ],
  (
    Controller,
    Fragment,
    jQuery,
    JSONModel,
    BusyIndicator,
    AjaxClient,
    Credentials
  ) => {
    return Controller.extend("VTCARS_PROJECT.controller.BaseController", {
      AjaxClient: AjaxClient,
      onInit() {},

      onPressHome: function () {
        let oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        oRouter.navTo("LandingPage");
      },

      onPressFavourites: function () {
        let oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        oRouter.navTo("Favourites");
      },

      onPressSellACar: function () {
        var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        oRouter.navTo("SellACar");
      },

      get: function (url) {
        return this.AjaxClient.get(url);
      },

      post: function (url, data) {
        return this.AjaxClient.post(url, data);
      },

      delete: function (url) {
        return this.AjaxClient.delete(url);
      },

      put: function (url, data) {
        return this.AjaxClient.put(url, data);
      },

      onPressMyProfile: function (oEvent) {
        const id = this.getUserId();

        this.get(
          "http://localhost:3000/user/" + id
        )
          .then((data) => {
            let userModel = new JSONModel(data);
            this.getView().setModel(userModel, "userModel");
          })
          .catch((err) => {
            console.log(err);
          });

        let oButton = oEvent.getSource();
        let oView = this.getView();

        if (!this._pPopover) {
          this._pPopover = Fragment.load({
            id: oView.getId(),
            name: "VTCARS_PROJECT.view.MyProfile",
            controller: this,
          }).then((oPopover) => {
            oView.addDependent(oPopover);
            

            return oPopover;
          });
        }
        this._pPopover.then(function (oPopover) {
          oPopover.openBy(oButton);
        });
      },

      handleViewListingsPress: function () {
        var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        oRouter.navTo("ViewHistory");
      },

      handleLogoutPress: function () {
        let view = this.getView();
        if (!this._lPop) {
          this._lPop = Fragment.load({
            id: view.getId(),
            name: "VTCARS_PROJECT.view.Logout",
            controller: this,
          }).then((oPop) => {
            this.getView().addDependent(oPop);
            return oPop;
          });
        }
        this._lPop.then(function (oPop) {
          oPop.open();
        });
      },

      handleCancelLogout: function () {
        this.byId("logoutDialog").close();
      },

      handleYesLogout: function () {
        var oRouter = this.getOwnerComponent().getRouter();
        document.cookie.split(";").forEach(function(c) { document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); });
        this.showBusyIndicator(1000, 0);
        oRouter.navTo("Login");
      },

      getRouter: function () {
        return sap.ui.core.UIComponent.getRouterFor(this);
      },

      getI18nMessage: function (sI18n, arg) {
        var oBundle = this.getView().getModel("i18n").getResourceBundle();
        var sMsg = oBundle.getText(sI18n, arg);
        return sMsg;
      },

      handleEditProfilePress() {
        var oRouter = this.getOwnerComponent().getRouter();
        oRouter.navTo("EditPage");
      },

      encryptPassword: function (password) {
        const secret = Credentials.getEncryptionSecret();
        const encryptedPassword = CryptoJS.AES.encrypt(
          password,
          secret
        ).toString();
        return encryptedPassword;
      },

      verifyToken: async function () {
        const oRouter = this.getOwnerComponent().getRouter();
        if (!document.cookie) {
          oRouter.navTo("Login");
          return;
        }

        this.post(
          "http://localhost:3000/auth"
        ).catch((err) => {
          console.log("Error" + err);
          oRouter.navTo("Login");
        });
      },

      hideBusyIndicator: function () {
        BusyIndicator.hide();
      },

      showBusyIndicator: function (iDuration, iDelay) {
        BusyIndicator.show(iDelay);

        if (iDuration && iDuration > 0) {
          if (this._sTimeoutId) {
            clearTimeout(this._sTimeoutId);
            this._sTimeoutId = null;
          }

          this._sTimeoutId = setTimeout(
            function () {
              this.hideBusyIndicator();
            }.bind(this),
            iDuration
          );
        }
      },

      getToken: function () {
        const cookieData = document.cookie.split(/[\ ;=]/);

        for (let i = 0; i < cookieData.length; i++) {
          if (cookieData[i] === "token") return cookieData[i + 1];
        }
      },

      getUserId: function () {
        const cookieData = document.cookie.split(/[\ ;=]/);

        for (let i = 0; i < cookieData.length; i++) {
          if (cookieData[i] === "id") return cookieData[i + 1];
        }
      },

      handleViewRatingPress: function(){
        this.getView().byId("ratingIndicatorMyProfile").setVisible(true)
        this.getView().byId("myProfileRatings").setVisible(true)

      },
    });
  }
);
