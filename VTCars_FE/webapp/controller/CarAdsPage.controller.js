sap.ui.define(
  [
    "VTCARS_PROJECT/controller/Base.controller",
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/Fragment",
    "sap/m/MessageToast",
    "jquery.sap.global",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
    "sap/ui/core/format/DateFormat",
  ],
  (
    BaseController,
    JSONModel,
    Fragment,
    MessageToast,
    jQuery,
    Filter,
    FilterOperator,
    DateFormat
  ) => {
    return BaseController.extend("VTCARS_PROJECT.controller.CarAdsPage", {
      onInit() {
        this.filters = [
          "bodyStyle",
          "model",
          "fuelType",
          "engineType",
          "capacity",
          "gearbox",
          "drivetrain",
          "isVerified",
          "priceTo",
          "priceFrom",
        ];

        let oRouter = this.getOwnerComponent().getRouter();
        oRouter.getRoute("CarAdsPage").attachMatched(this.carAdsPageFunction, this);

        this.byId("yearF").setMaxDate(new Date());
        this.byId("yearF").setMinDate(new Date("2000"));
        this.byId("yearT").setMaxDate(new Date());

        this.get("http://localhost:3000/getBrandModel")
          .then((dataBrand) => {
            let brandM = new JSONModel(dataBrand);
            this.getView().setModel(brandM, "brandM");
          })
          .catch((err) => {
            console.log(err);
          });
      },

      onApplyFilters() {
        this.post(
          "http://localhost:3000/cars",
          this.getOwnerComponent().getModel("filteredBy").getData()
        )
          .then((data) => {
            let carAds = new JSONModel(data);
            this.getView().setModel(carAds, "carAds");
          })
          .catch((err) => {
            console.log(err.statusText);
            let carAds = new JSONModel();
            this.getView().setModel(carAds, "carAds");
          });
      },
      onClearFilters() {
        this.filters.forEach((filter) => {
          this.getView()
            .byId(`combo${[filter]}Listing`)
            .clearSelection();
        });
        this.byId("comboBrandListing").clearSelection();
        this.byId("yearF").setValue(null);
        this.byId("yearT").setValue(null);
        this.byId("combomaxMileageListing").setValue("");
      },

      onChangeDateFrom(oEvent) {
        const dt = DateFormat.getDateTimeInstance({ pattern: "yyyy" });
        const jsDateObject = dt.parse(this.byId("yearF").getProperty("value"));
        const year = dt.format(jsDateObject);
        const yearDate = new Date(year);
        this.byId("yearT").setMinDate(yearDate);
        let fromKey = oEvent.getParameter("value");
        this.getOwnerComponent()
          .getModel("filteredBy")
          .setProperty("/yearFrom", fromKey);
      },

      onChangeDateTo(oEvent) {
        let toKey = oEvent.getParameter("value");
        this.getOwnerComponent()
          .getModel("filteredBy")
          .setProperty("/yearTo", toKey);
      },

      carAdsPageFunction: function () {
        this.verifyToken();
        this.keepBrand();
        this.keepFilters();
        this.keepDates();
        this.keepMileage();
        this.getModelsFromSelectedKey();
        this.onApplyFilters();
        this.byId("yearF").setMaxDate(new Date());
        this.byId("yearF").setMinDate(new Date("2000"));
        this.byId("yearT").setMaxDate(new Date());

        this.get("http://localhost:3000/getBrandModel")
          .then((dataBrand) => {
            let brandM = new JSONModel(dataBrand);
            this.getView().setModel(brandM, "brandM");
          })
          .catch((err) => {
            console.log(err);
          });
      },

      onPressHomeListing() {
        this.getOwnerComponent().getModel("filteredBy").setData({});
        let oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        oRouter.navTo("LandingPage");
      },

      keepDates: function () {
        this.byId("yearF").setValue(
          this.getOwnerComponent().getModel("filteredBy").getData().yearFrom
        );
        this.byId("yearT").setValue(
          this.getOwnerComponent().getModel("filteredBy").getData().yearTo
        );
      },

      keepMileage: function () {
        this.byId("combomaxMileageListing").setValue(
          this.getOwnerComponent().getModel("filteredBy").getData().maxMileage
        );
      },

      keepFilters: function () {
        this.filters.forEach((filter) => {
          const key = this.getOwnerComponent().getModel("filteredBy").getData()[
            filter
          ];
          this.getView()
            .byId(`combo${[filter]}Listing`)
            .setSelectedKey(key);
        });
      },

      keepBrand: function () {
        let modelFilter = new JSONModel();
        this.getView().setModel(modelFilter, "modelFilter");
        let brandKey = this.getOwnerComponent()
          .getModel("filteredBy")
          .getData().brand;
        this.getView().byId("comboBrandListing").setSelectedKey(brandKey);
      },

      getModelsFromSelectedKey: async function () {
        if (this.getView().getModel("filteredBy").getData().brand) {
          this.getView().setBusy(true);
          let selectedKey = this.getView()
            .getModel("filteredBy")
            .getData().brand;
          let response = await fetch("http://localhost:3000/getBrandModel", {
            credentials: "include",
          });
          let data = await response.json();
          let brandObject = data.find(
            (entry) => entry.brandName === selectedKey
          );
          this.byId("comboBrandListing").fireSelectionChange(brandObject);
          this.getView().setBusy(false);
        }
      },

      onSelectionBrand(oEvent) {
        if (this.getView().byId("comboBrandListing").getSelectedItem()) {
          let modelArray = oEvent
            .getSource()
            .getSelectedItem()
            .getBindingContext("brandM")
            .getObject().models;
          this.getView().getModel("modelFilter").setData(modelArray);
        }
        let brandKey = this.getView()
          .byId("comboBrandListing")
          .getSelectedKey();
        this.getOwnerComponent()
          .getModel("filteredBy")
          .setProperty("/brand", brandKey);
        if (this.getView().byId("comboBrandListing").getSelectedKey() == "") {
          this.getView().byId("combomodelListing").clearSelection();
          this.getView().getModel("modelFilter").setData({});
        }
      },

      isNumberFieldValid: function (testNumber) {
        let isNum = /^[0-9\s]*$/.test(testNumber); // test for numbers or empty strings only
        return isNum;
      },

      onSubmitMileage() {
        let key = this.getView().byId("combomaxMileageListing").getValue();
        if (this.isNumberFieldValid(key)) {
          this.byId("combomaxMileageListing").setValueState(
            sap.ui.core.ValueState.None
          );
          this.getOwnerComponent()
            .getModel("filteredBy")
            .setProperty("/maxMileage", key);
        } else {
          this.byId("combomaxMileageListing").setValueState(
            sap.ui.core.ValueState.Error
          );
        }
      },

      onSelectionChange(comboId, filteredByField) {
        let key = this.getView().byId(comboId).getSelectedKey();
        this.getOwnerComponent()
          .getModel("filteredBy")
          .setProperty(`/${filteredByField}`, key);
      },

      onSearch(oEvent) {
        let sQuery = oEvent.getParameter("query");

        this.get("http://localhost:3000/car/brand/" + sQuery)
          .then((data) => {
            let carAds = new JSONModel(data);
            this.getView().setModel(carAds, "carAds");
          })
          .catch((err) => {
            console.log(err);
          });

        this.byId("searchFieldListing").clearSelection();
      },

      onSuggest: function (event) {
        let sValue = event.getParameter("suggestValue");
        let aFilters = [];
        let filter = sValue.toUpperCase();
        if (sValue) {
          aFilters = [
            new Filter(
              new Filter("brandName", function (sText) {
                return (sText || "").toUpperCase().indexOf(filter) > -1; // either sText or "" containts the given filter value as a substring
                //indexOf() method returns the position of the first occurrence of the specified character or string in a specified string.
              })
            ),
          ];
        }
        this.byId("searchFieldListing")
          .getBinding("suggestionItems")
          .filter(aFilters);
        this.byId("searchFieldListing").suggest();
      },

      onPressSeeMore: function (oEvent) {
        let selectedCar = oEvent.getSource().getBindingContext("carAds").getObject();
        this.getOwnerComponent().getModel("selectedCar").setData(selectedCar);

        this.get("http://localhost:3000/car/" + this.getView().getModel("selectedCar").getData()._id)
          .then((response) => {
            let selectedCarId = this.getView().getModel("selectedCar").getProperty('/_id');
            this.getRouter().navTo("Details", { selectedId: selectedCarId });
          })
          .catch((err) => {
            console.log(err);
          });
      },
    });
  }
);