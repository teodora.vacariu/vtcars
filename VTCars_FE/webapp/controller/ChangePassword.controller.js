sap.ui.define(
    [
      "VTCARS_PROJECT/controller/Base.controller",
      "sap/ui/model/json/JSONModel",
      "sap/m/MessageToast",
      "VTCARS_PROJECT/utils/Validations",
      "sap/m/MessageBox",
    ],
    (BaseController, JsonModel, MessageToast, Validations, MessageBox) => {
      return BaseController.extend("VTCARS_PROJECT.controller.ChangePassword", {
        onInit() {
          const oRouter = this.getOwnerComponent().getRouter();
          oRouter
            .getRoute("ChangePassword")
            .attachMatched(this.changePasswordFunction, this);
        },
        changePasswordFunction() {
          let updatePasswordModel = new JsonModel();
          this.getView().setModel(updatePasswordModel, "updatePasswordModel");
  
          let stateModel = new JsonModel({
            new_passwordState: "None",
            confirm_new__passwordState: "None",
          });
          this.getView().setModel(stateModel, "stateModel");
        },
  
        checkPassword: function () {
          let userCheck = this.getView().getModel("updatePasswordModel").getData();
          let password = userCheck.new_password;
          if (Validations.validatePassword(password)) {
            this.getView()
              .getModel("stateModel")
              .setProperty("/new_passwordState", "None");
          } else {
            this.getView()
              .getModel("stateModel")
              .setProperty("/new_passwordState", "Error");
          }
        },
  
        checkConfirmPassword: function () {
          let userCheck = this.getView().getModel("updatePasswordModel").getData();
          let confirmPassword = userCheck.confirm_new_password;
          if (confirmPassword) {
            this.getView()
              .getModel("stateModel")
              .setProperty("/confirm_new__passwordState", "None");
          } else {
            this.getView()
              .getModel("stateModel")
              .setProperty("/confirm_new__passwordState", "Error");
          }
        },
        onPressApply: async function () {
          if (
            this.getView().getModel("stateModel").getData()
              .confirm_new__passwordState === "Error" ||
            this.getView().getModel("stateModel").getData().new_passwordState ===
              "Error"
          ) {
            MessageBox.error(this.getI18nMessage("message_box_incorrect_input"));
          } else {
            const data = this.getView().getModel("updatePasswordModel").getData();
            data.new_password = this.encryptPassword(data.new_password);  
            try {
              const response = await this.put(
                "http://localhost:3000/user/changePassword",
                data
              );
              if (response) {
  
                MessageToast.show(response.message);
                this.getView().getModel("updatePasswordModel").setData({});
                document.cookie = "emailToken= ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
              }
            } catch (err) {
              if(err.status === 401) {
                MessageBox.error(this.getI18nMessage("EMAIL_ERROR"));
              }
              else if (err.status === 409 || err.status === 400) {
                MessageBox.error(err.responseJSON.error);
              } 
              else {
                MessageBox.error(err.statusText);
                this.getView().getModel("updatePasswordModel").setData({});
              }
            }
          }
        },
      });
    }
  );
  