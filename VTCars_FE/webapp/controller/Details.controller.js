sap.ui.define(
  [
    "VTCARS_PROJECT/controller/Base.controller",
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/Fragment",
    "sap/ui/core/routing/History",
    "sap/m/MessageBox",
    "sap/m/MessageToast",
    "sap/ui/integration/library",
    "sap/ui/core/format/DateFormat",
  ],

  (BaseController, JSONModel, Fragment, History, MessageBox, MessageToast, integrationLibrary, DateFormat) => {
    return BaseController.extend("VTCARS_PROJECT.controller.CarAdsPage", {
      onInit() {
        const oRouter = this.getOwnerComponent().getRouter();
        oRouter
          .getRoute("Details")
          .attachMatched(this.detailsFunctionality, this);
      },

      onPressSubmitRate: function () {
        let sellerID = this.getOwnerComponent().getModel("selectedCar").getData().owner;
        let stars = this.byId("ratingIndicator").getValue();
        this.post(`http://localhost:3000/rate`,"stars="+ stars +"&sellerID=" + sellerID)
          .then(data => {
            let ratedUser = new JSONModel({});
            this.getView().setModel(ratedUser, "ratedUser");
            this.byId("rateSellerPopover").close();
            this.getView().getModel('ownerModel').setProperty('/ratingMean', this.userModel.getProperty('/ratingMean'));
            this.getView().getModel('ownerModel').updateBindings();
          }) 
          .catch((err) => {
            console.log(err)
          })
      },
    
      addComment: function(){
        let selectedCar = this.getOwnerComponent().getModel("selectedCar").getData()._id;
        let comment = String(this.byId("feedInput").getValue());
        this.post(`http://localhost:3000/carAd/comment`,"selectedCar=" + selectedCar + "&comment=" + comment)
          .then((data) => {
            this.get("http://localhost:3000/car/" +
            this.getOwnerComponent().getModel("selectedCar").getData()._id)
              .then(response => {
                this.getOwnerComponent().getModel("selectedCar").setData(response);
                this.getOwnerComponent().getModel("commentsModel").setData(this.getOwnerComponent().getModel("selectedCar").getData().comments);
              })
              .catch((err) => {
                console.log(err);
              });
          }).catch((err) => {
            console.log(err)
          })
      },

      addToFavourites: function (selectedAd) {
        this.post(`http://localhost:3000/add-to-favourites/${selectedAd}`)
          .then((data) => {

            let favouriteCars = new JSONModel(data);
            this.getOwnerComponent().setModel(favouriteCars, "favouriteCars");
            MessageToast.show(this.getI18nMessage("SUCCESS_FAVOURITES"));
          })
          .catch((err) => {
            console.log(err);
          });
      },

      detailsFunctionality: function (oEvent) {
        this.verifyToken();
        this.byId("heartToggle").setType("Default");
        let id = this.getUserId();
        this.get("http://localhost:3000/user/" + id)
          .then((data) => {
            let userModel = new JSONModel(data);
            this.getView().setModel(userModel, "userModel");
          })
          .catch((err) => {
            console.log(err);
          });
        
        let uploadDateModel = new JSONModel();
        this.getView().setModel(uploadDateModel, "uploadDateModel");

        this.onVerifyHeart();

        this.get("http://localhost:3000/car/" + oEvent.getParameter("arguments").selectedId)
          .then((response) => {
            this.getView().getModel("selectedCar").setData(response);
            let commentsModel = new JSONModel();
            this.getView().setModel(commentsModel, "commentsModel");
            this.getView().getModel("commentsModel").setData(this.getOwnerComponent().getModel("selectedCar").getData().comments);
            this.setOwnerModel();
            let uploadDate = this.getView().getModel("selectedCar").getData().createdAt.split("T")[0];
            this.getView().getModel("uploadDateModel").setData(uploadDate);
          })
          .catch((err) => {
            console.log(err);
          });
        },

      onVerifyHeart: function () {
        const userId = this.getUserId();
        let selectedAd = this.getView().getModel("selectedCar").getData()._id;
        this.get("http://localhost:3000/user/" + userId)
          .then((data) => {
            data.favourites.forEach((element) => {
              if (element == selectedAd) {
                this.byId("heartToggle").setType("Reject");
              }
            });
          })
          .catch((err) => {
            console.log(err);
          });
      },

      setOwnerModel() {
        let ownerId = this.getOwnerComponent().getModel("selectedCar").getData().owner;
        this.get("http://localhost:3000/user/" + ownerId)
          .then((data) => {
            let ownerModel = new JSONModel(data);
            this.getView().setModel(ownerModel, "ownerModel");
          })
          .catch((err) => {
            console.log(err);
          });
      },

      getRouter: function () {
        return UIComponent.getRouterFor(this);
      },

      onPressShowSeller: function (oEvent) {
        this.setOwnerModel()
        let oButton = oEvent.getSource();
        let oViewSeller = this.getView();
        if (!this._sPopover) {
          this._sPopover = Fragment.load({
            id: "sellerPopover",
            name: "VTCARS_PROJECT.view.ContactInfo",
            controller: this,
          }).then((osPopover) => {
            oViewSeller.addDependent(osPopover);
            return osPopover;
          });
        }
        this._sPopover.then(function (osPopover) {
          osPopover.openBy(oButton);
        });
      },

      onPressBack: function () {
        let oHistory = History.getInstance();
        let sPreviousHash = oHistory.getPreviousHash();

        if (sPreviousHash !== undefined) {
          window.history.go(-1);
        } else {
          let oRouter = sap.ui.core.UIComponent.getRouterFor(this);
          oRouter.navTo("CarAdsPage", {}, true);
        }
      },

      onPressShowContactInfo: function (oEvent) {
        let oButton = oEvent.getSource();
        let oView = this.getView();
        if (!this._pPopover) {
          this._pPopover = Fragment.load({
            id: "profilePopover",
            name: "VTCARS_PROJECT.view.MyProfile",
            controller: this,
          }).then((oPopover) => {
            oView.addDependent(oPopover);
            return oPopover;
          });
        }
        this._pPopover.then(function (oPopover) {
          oPopover.openBy(oButton);
        });
      },

      onPressRateSeller: function (oEvent) {
        let oButton = oEvent.getSource();
        let oView = this.getView();
        if (!this._pPopover) {
          this._pPopover = Fragment.load({
            id: oView.getId(),
            name: "VTCARS_PROJECT.view.RateSeller",
            controller: this,
          }).then((oPopover) => {
            oView.addDependent(oPopover);
            return oPopover;
          });
        }
        this._pPopover.then(function (oPopover) {
          oPopover.openBy(oButton);
        });
      },

      onPressHeart: function () {
        let value = this.byId("heartToggle").getType();
        if (value == "Default") {
          let selectedAd = this.getView().getModel("selectedCar").getData()._id;
          this.get("http://localhost:3000/userr/favourites")
            .then((data) => {
              this.addToFavourites(selectedAd);
              this.byId("heartToggle").setType("Reject")
            })
            .catch((err) => {
              console.log(err);
            });
        }
        else {
          let id = this.getView().getModel("selectedCar").getData()._id;
          this.delete("http://localhost:3000/fav/" + id)
            .then((response) => {
              this.byId("heartToggle").setType("Default")
              MessageToast.show(this.getI18nMessage("REMOVE_FAVOURITESPOST"));
            })
            .catch((err) => {
              console.log("err", err);
            });
        }
      },

      onPressDownload: async function () {
        const car = this.getView().getModel("selectedCar").getData();
        if (car.isVerified == true) {
          window.open(`http://localhost:3000/document/${car.documents[0]}`, "_tab");
        } else {
          MessageBox.information(this.getI18nMessage("NODOCUMENT"));
        }

      },
    });
  }
);
