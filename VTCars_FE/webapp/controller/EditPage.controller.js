sap.ui.define(
  [
    "VTCARS_PROJECT/controller/Base.controller",
    "sap/ui/model/json/JSONModel",
    "sap/m/MessageBox",
    "sap/m/MessageToast",
    "sap/ui/core/Fragment",
    "VTCARS_PROJECT/utils/Validations",
  ],
  (BaseController, JSONModel, MessageBox, MessageToast, Fragment, Validations) => {
    return BaseController.extend("VTCARS_PROJECT.controller.EditPage", {
      onInit() {
        const oRouter = this.getOwnerComponent().getRouter();
        oRouter.getRoute("EditPage").attachMatched(this.editPageFunction, this);
      },

      editPageFunction() {

        this.verifyToken();

        let stateModel = new JSONModel({
          cityState: "None",
          phoneNumberState: "None",
          firstNameState: "None",
          lastNameState: "None",
          emailState: "None",
          
        });
        this.getView().setModel(stateModel, "stateModel");
        this.stateModel=this.getView().getModel('stateModel');

        const id = this.getUserId();
        this.get("http://localhost:3000/user/" + id)
          .then((data) => {
            let userModel = new JSONModel(data);
            this.getView().setModel(userModel, "userModel");
          })
          .catch((err) => {
            console.log(err);
          });
      },

      onPressSave() {
        if (this.stateModel.getProperty('/cityState') === "Error" ||
        this.stateModel.getProperty('/phoneNumberState') === "Error" ||
        this.stateModel.getProperty('/emailState') === "Error" ||
        this.stateModel.getProperty('/firstNameState') === "Error" ||
        this.stateModel.getProperty('/lastNameState') === "Error" ||
        this.stateModel.getProperty('/emailState') === "Error") {
          MessageBox.error(this.getI18nMessage("message_box_incorrect_input"));
        } else {
          let data = this.getView().getModel("userModel").getData();
          this.put("http://localhost:3000/user/update/", data)
            .then(updateData => {
              this.showBusyIndicator(1000, 0);
              MessageToast.show(this.getI18nMessage("message_box_updated"));
              this.getView().getModel("userModel").setData(updateData);
              const oRouter = this.getOwnerComponent().getRouter();
              oRouter.navTo("LandingPage");
            })
            .catch((err) => {
              console.log(err);
            });
        }
      },

      onChangeNumber() {
        let userCheck = this.getView().getModel("userModel").getData();
        let phoneNumber = userCheck.phoneNumber;
        if (Validations.validatePhone(phoneNumber)) {
          this.getView().getModel("stateModel").setProperty("/phoneNumberState", "None");
        } else {
          this.getView().getModel("stateModel").setProperty("/phoneNumberState", "Error");
        }
      },

      onChangeCity() {
        let userCheck = this.getView().getModel("userModel").getData();
        let city = userCheck.city;
        if (Validations.validateName(city)) {
          this.getView().getModel("stateModel").setProperty("/cityState", "None");
        } else {
          this.getView().getModel("stateModel").setProperty("/cityState", "Error");
        }
      },

      onChangeEmail() {
        let userCheck = this.getView().getModel("userModel").getData();
        let email = userCheck.email;
        if (Validations.validateEmail(email)) {
          this.getView().getModel("stateModel").setProperty("/emailState", "None");
        } else {
          this.getView().getModel("stateModel").setProperty("/emailState", "Error");
        }

      },
      onChangeLastName(){
        let userCheck = this.getView().getModel("userModel").getData();
        let lastName = userCheck.lastName;
        if (Validations.validateName(lastName)) {
          this.getView().getModel("stateModel").setProperty("/lastNameState", "None");
        } else {
          this.getView().getModel("stateModel").setProperty("/lastNameState", "Error");
        }
      },

      onChangeFirstName(){
        let userCheck = this.getView().getModel("userModel").getData();
        let firstName = userCheck.firstName;
        if (Validations.validateName(firstName)) {
          this.getView().getModel("stateModel").setProperty("/firstNameState", "None");
        } else {
          this.getView().getModel("stateModel").setProperty("/firstNameState", "Error");
        }
      },

      onPressCancel() {
        const oRouter = this.getOwnerComponent().getRouter();
        oRouter.navTo("LandingPage");
      },

      onPressChangePassword: async function() {
        try {
          const response = await this.post("http://localhost:3000/sendEmail/", {});
          MessageToast.show(this.getI18nMessage("email_sent"));
          document.cookie = `emailToken=${response.emailToken}`
        } catch(err) {
          console.log(err);
        }

      }
    
    });
  }
);
