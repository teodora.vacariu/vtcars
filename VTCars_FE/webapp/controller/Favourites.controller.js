sap.ui.define(
  [
    "VTCARS_PROJECT/controller/Base.controller",
    "jquery.sap.global",
    "sap/ui/core/Fragment",
    "sap/m/MessageToast"
  ],
  (BaseController, jQuery, Fragment, MessageToast) => {
    return BaseController.extend("VTCARS_PROJECT.controller.Favourites", {
      onInit() {
        const oRouter = this.getOwnerComponent().getRouter();
        oRouter.getRoute("Favourites").attachMatched(this.onRouteMatched, this);
        
      },

      onRouteMatched: async function () {
        this.verifyToken();
        this.showBusyIndicator(2000, 0);
        let resArray = [];
        const data = await this.get("http://localhost:3000/userr/favourites")
          .then((data) => {
            if (data.length > 0) {
              resArray = [...data];
              this.getOwnerComponent()
                .getModel("favouriteCars")
                .setData(resArray);
            } else {
              this.getOwnerComponent().getModel("favouriteCars").setData({});
            }
          })
          .catch((err) => {
            console.log(err);
          });
          
      },

      onPressSeeMoreFav: function (oEvent) {
        let selectedCar = oEvent
          .getSource()
          .getBindingContext("favouriteCars")
          .getObject();
        this.getView().getModel("selectedCar").setData(selectedCar);

        this.get(
          "http://localhost:3000/car/" +
            this.getView().getModel("selectedCar").getData()._id
        )

          .then((response) => {
            let selectedCarId = this.getView()
              .getModel("selectedCar")
              .getData()._id;

            this.getRouter().navTo("Details", { selectedId: selectedCarId });
          })

          .catch((err) => {
            console.log(err);
          });
      },

      onPressRemoveFromFavourites: function (oEvent) {
        let selectedCar = oEvent
          .getSource()
          .getBindingContext("favouriteCars")
          .getObject();
        this.getView().getModel("selectedCar").setData(selectedCar);
        let oViewF = this.getView();
        if (!this.byId("removeDialog")) {
          Fragment.load({
            id: oViewF.getId(),
            name: "VTCARS_PROJECT.view.RemoveFavourite",
            controller: this,
          }).then(function (oDialogF) {
            oViewF.addDependent(oDialogF);
            oDialogF.open();
          });
        } else {
          this.byId("removeDialog").open();
        }
      },

      handleYesDelete: async function () {
        let id = this.getView().getModel("selectedCar").getData()._id;
        this.delete("http://localhost:3000/fav/" + id)
          .then((response) => {
            this.onRouteMatched();
            this.byId("removeDialog").close();
            MessageToast.show(this.getI18nMessage("REMOVE_FAVOURITESPOST"))
          })
          .catch((err) => {
            console.log("err", err); 
          });
      },

      handleCancelDelete: function () {
        this.byId("removeDialog").close();
      },
    });
  }
);
