sap.ui.define(
  [
    "VTCARS_PROJECT/controller/Base.controller",
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/Fragment",
    "jquery.sap.global",
    "sap/ui/core/format/DateFormat",
  ],
  (BaseController, JSONModel, Fragment, jQuery, DateFormat) => {
    return BaseController.extend("VTCARS_PROJECT.controller.LandingPage", {
      getRouter: function () {
        return this.getOwnerComponent().getRouter();
      },
      onInit() {
        const oRouter = this.getOwnerComponent().getRouter();
        oRouter
          .getRoute("LandingPage")
          .attachMatched(this.landingPageFunction, this);

        let modelFilter = new JSONModel();
        this.getView().setModel(modelFilter, "modelFilter");

        this.get("http://localhost:3000/getBrandModel")
          .then((dataBrand) => {
            let brandM = new JSONModel(dataBrand);
            this.getView().setModel(brandM, "brandM");
          })
          .catch((err) => {
            console.log(err);
          });
      },

      landingPageFunction: function () {
        this.verifyToken();
        this.getOwnerComponent().getModel("filteredBy").setData({});
      },

      onSelectionBrand(oEvent) {
        if (this.getView().byId("comboBrandLanding").getSelectedItem()) {
          let modelArray = oEvent
            .getSource()
            .getSelectedItem()
            .getBindingContext("brandM")
            .getObject().models;
          this.getView().getModel("modelFilter").setData(modelArray);
        }
        let brandKey = this.getView()
          .byId("comboBrandLanding")
          .getSelectedKey();
        this.getOwnerComponent()
          .getModel("filteredBy")
          .setProperty("/brand", brandKey);
        if (this.getView().byId("comboBrandLanding").getSelectedKey() == "") {
          this.getView().byId("comboModelLanding").clearSelection();
          this.getView().getModel("modelFilter").setData({});
        }
      },

      onSelection(comboBoxModel, filteredByModel) {
        let key = this.getView().byId(comboBoxModel).getSelectedKey();
        this.getOwnerComponent()
          .getModel("filteredBy")
          .setProperty(`/${filteredByModel}`, key);
      },

      onChangeDate(oEvent) {
        const dt = DateFormat.getDateTimeInstance({ pattern: "yyyy" });
        const jsDateObject = dt.parse(this.byId("yearF").getProperty("value"));
        const year = dt.format(jsDateObject);
        const yearDate = new Date(year);
        this.byId("yearT").setMinDate(yearDate);
        this.byId("yearT").setMaxDate(new Date());
        let fromKey = oEvent.getParameter("value");
        this.getOwnerComponent()
          .getModel("filteredBy")
          .setProperty("/yearFrom", fromKey);
      },

      onChangeDate2(oEvent) {
        let toKey = oEvent.getParameter("value");
        this.getOwnerComponent()
          .getModel("filteredBy")
          .setProperty("/yearTo", toKey);
      },

      onSelectionSlider(oEvent) {
        let sliderKey = oEvent.getParameter("value");
        this.getOwnerComponent()
          .getModel("filteredBy")
          .setProperty("/maxMileage", sliderKey);
      },

      onPressMoreFilters() {
        let oView = this.getView();

        if (!this.byId("filterWindow")) {
          Fragment.load({
            id: oView.getId(),
            name: "VTCARS_PROJECT.view.Filters",
            controller: this,
          }).then(function (oDialog) {
            oView.byId("comboMileage").setValue(0);
            oView.addDependent(oDialog);
            oDialog.open();
          });
        } else {
          this.byId("filterWindow").open();
          oView.byId("comboMileage").setValue(0);
        }
      },

      onPressMoreFiltersCancel() {
        this.byId("filterWindow").close();
      },

      onPressMoreFiltersApply() {
        this.onPressGoToCarAdsPage();
      },

      onPressListing: function () {
        this.onPressGoToCarAdsPage();
      },

      onPressGoToCarAdsPage: function () {
        let oRouter = sap.ui.core.UIComponent.getRouterFor(this);
        oRouter.navTo("CarAdsPage");
      },

      onPressTile: function (comboId, filteredByField) {
        this.getOwnerComponent().getModel("filteredBy").setData({});
        this.getOwnerComponent()
          .getModel("filteredBy")
          .setProperty(`/${filteredByField}`, comboId);
        this.onPressGoToCarAdsPage();
      },
    });
  }
);