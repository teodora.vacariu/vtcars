sap.ui.define(
  [
    "VTCARS_PROJECT/controller/Base.controller",
    "sap/ui/model/json/JSONModel",
    "sap/m/MessageToast",
    "sap/m/MessageBox",
    "VTCARS_PROJECT/utils/Validations"
  ],
  (BaseController, JsonModel, MessageToast, MessageBox,Validations) => {
    return BaseController.extend("VTCARS_PROJECT.controller.Login", {
      onInit() {
        let newUser = new JsonModel();
        this.getView().setModel(newUser, "newUser");
      },

      onRegisterHerePress: function (evt) {
        let newUser = new JsonModel();
        this.getView().setModel(newUser, "newUser");
        let oRouter = this.getOwnerComponent().getRouter();
        oRouter.navTo("Register");
      },

      checkEmail: function () {
        let userCheck = this.getView().getModel("newUser").getData();
        let email = userCheck.email;
        if (Validations.validateEmail(email)) {
          this.getView().getModel("newUser").setProperty("/emailState", "None");
        } else {
          this.getView()
            .getModel("newUser")
            .setProperty("/emailState", "Error");
        }
      },

      checkPassword: function () {
        let userCheck = this.getView().getModel("newUser").getData();
        let password = userCheck.password;
        if (password) {
          this.getView()
            .getModel("newUser")
            .setProperty("/passwordState", "None");
        } else {
          this.getView()
            .getModel("newUser")
            .setProperty("/passwordState", "Error");
        }
      },

      onLogin: function () {
        const data = this.getView().getModel("newUser").oData;
        const email = data.email;
        const password = data.password;
        if (Validations.validateEmail(email) && password) {
          
          const dataI = JSON.parse(JSON.stringify(data));
          dataI.password = this.encryptPassword(data.password);

          this.post("http://localhost:3000/login", dataI)
            .then((response) => {   
              MessageToast.show(this.getI18nMessage("LOGIN_SUCCESSFUL"));      
              let newUser = new JsonModel();
              this.getView().setModel(newUser, "newUser");    
              document.cookie = `token=${response.token}`;
              document.cookie = `id=${response._id}`;
              let oRouter = this.getOwnerComponent().getRouter();
              oRouter.navTo("LandingPage");

              this.getView().getModel("newUser").setProperty("/emailState", "None");
              this.getView()
                .getModel("newUser")
                .setProperty("/passwordState", "None");
            })
            .catch((err) => {       
              console.log(err);
              MessageBox.error(this.getI18nMessage("EMAIL_PASSWORD_ERROR"),
                {
                  title: this.getI18nMessage("LOGIN_FAILED"),
                  onClose: null,
                  styleClass: "",
                  actions: sap.m.MessageBox.Action.OK,
                  emphasizedAction: null,
                  initialFocus: null,
                  textDirection: sap.ui.core.TextDirection.Inherit,
                }
              );
            });
         
        } else {
          sap.m.MessageBox.error(this.getI18nMessage("HIGHLIGHTED_FIELDS"), {
            title: this.getI18nMessage("LOGIN_FAILED"),
            onClose: null,
            styleClass: "",
            actions: sap.m.MessageBox.Action.OK,
            emphasizedAction: null,
            initialFocus: null,
            textDirection: sap.ui.core.TextDirection.Inherit,
          });
          if (!Validations.validateEmail(email)) {
            this.getView()
              .getModel("newUser")
              .setProperty("/emailState", "Error");
          }
          if (!password) {
            this.getView()
              .getModel("newUser")
              .setProperty("/passwordState", "Error");
          }
        }
      },
    });
  }
);
