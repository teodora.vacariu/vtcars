sap.ui.define(
  [
    "VTCARS_PROJECT/controller/Base.controller",
    "sap/ui/model/json/JSONModel",
    "sap/m/MessageToast",
    "sap/m/MessageBox",
    "VTCARS_PROJECT/utils/Validations",
  ],
  (BaseController, JsonModel, MessageToast, MessageBox, Validations) => {

    return BaseController.extend("VTCARS_PROJECT.controller.Register", {
      onInit() {
        let newUser = new JsonModel({
          county: "Alba",
        });
        this.getView().setModel(newUser, "newUser");

        let oData = {
          SelectedCounty: "UserCounty",
          CountyCollection: [
            { StatusId: "Alba", Status: "Alba" }, { StatusId: "Arad", Status: "Arad" }, { StatusId: "Arges", Status: "Arges" }, { StatusId: "Bacau", Status: "Bacau" },
            { StatusId: "Bihor", Status: "Bihor" }, { StatusId: "Bistrita-Nasaud", Status: "Bistrita-Nasaud" }, { StatusId: "Botosani", Status: "Botosani" }, { StatusId: "Brasov", Status: "Brasov" },
            { StatusId: "Braila", Status: "Braila" }, { StatusId: "Bucuresti", Status: "Bucuresti" }, { StatusId: "Buzau", Status: "Buzau" }, { StatusId: "Caras-Severin", Status: "Caras-Severin" },
            { StatusId: "Calarasi", Status: "Calarasi" }, { StatusId: "Cluj", Status: "Cluj" }, { StatusId: "Constanta", Status: "Constanta" }, { StatusId: "Covasna", Status: "Covasna" },
            { StatusId: "Dambovita", Status: "Dambovita" }, { StatusId: "Dolj", Status: "Dolj" }, { StatusId: "Galati", Status: "Galati" }, { StatusId: "Giurgiu", Status: "Giurgiu" },
            { StatusId: "Gorj", Status: "Gorj" }, { StatusId: "Harghita", Status: "Harghita" }, { StatusId: "Hunedoara", Status: "Hunedoara" }, { StatusId: "Ialomita", Status: "Ialomita" },
            { StatusId: "Iasi", Status: "Iasi" }, { StatusId: "Ilfov", Status: "Ilfov" }, { StatusId: "Maramures", Status: "Maramures" }, { StatusId: "Mehedinti", Status: "Mehedinti" },
            { StatusId: "Mures", Status: "Mures" }, { StatusId: "Neamt", Status: "Neamt" }, { StatusId: "Olt", Status: "Olt" }, { StatusId: "Prahova", Status: "Prahova" },
            { StatusId: "Satu Mare", Status: "Satu Mare" }, { StatusId: "Salaj", Status: "Salaj" }, { StatusId: "Sibiu", Status: "Sibiu" }, { StatusId: "Suceava", Status: "Suceava" },
            { StatusId: "Teleorman", Status: "Teleorman" }, { StatusId: "Timis", Status: "Timis" }, { StatusId: "Tulcea", Status: "Tulcea" }, { StatusId: "Vaslui", Status: "Vaslui" },
            { StatusId: "Valcea", Status: "Valcea" }, { StatusId: "Vrancea", Status: "Vrancea" },
          ],
          Editable: true,
          Enabled: true,
        };
        let oModel = new JsonModel(oData);
        this.getView().setModel(oModel, "SelectedModel");
      },

      onLoginHerePress: function (evt) {
        let newUser = new JsonModel();
        this.getView().setModel(newUser, "newUser");
        let oRouter = this.getOwnerComponent().getRouter();
        oRouter.navTo("Login");
      },

      
      checkFirstName: function () {
        let userCheck = this.getView().getModel("newUser").getData();
        let firstName = userCheck.firstName;
        if (Validations.validateName(firstName)) {
          this.getView().getModel("newUser").setProperty("/firstNameState", "None");
        } else {
          this.getView().getModel("newUser").setProperty("/firstNameState", "Error");
        }
      },

      checkLastName: function () {
        let userCheck = this.getView().getModel("newUser").getData();
        let lastName = userCheck.lastName;
        if (Validations.validateName(lastName)) {
          this.getView().getModel("newUser").setProperty("/lastNameState", "None");
        } else {
          this.getView().getModel("newUser").setProperty("/lastNameState", "Error");
        }
      },

      checkPhoneNumber: function () {
        let userCheck = this.getView().getModel("newUser").getData();
        let phoneNumber = userCheck.phoneNumber;
        if (Validations.validatePhone(phoneNumber)) {
          this.getView().getModel("newUser").setProperty("/phoneNumberState", "None");
        } else {
          this.getView().getModel("newUser").setProperty("/phoneNumberState", "Error");
        }
      },

      checkEmail: function () {
        let userCheck = this.getView().getModel("newUser").getData();
        let email = userCheck.email;
        if (Validations.validateEmail(email)) {
          this.getView().getModel("newUser").setProperty("/emailState", "None");
        } else {
          this.getView().getModel("newUser").setProperty("/emailState", "Error");
        }
      },

      checkCity: function () {
        let userCheck = this.getView().getModel("newUser").getData();
        let city = userCheck.city;
        if (city) {
          this.getView().getModel("newUser").setProperty("/cityState", "None");
        } else {
          this.getView().getModel("newUser").setProperty("/cityState", "Error");
        }
      },

      checkPassword: function () {
        let userCheck = this.getView().getModel("newUser").getData();
        let password = userCheck.password;
        if (Validations.validatePassword(password)) {
          this.getView().getModel("newUser").setProperty("/passwordState", "None");
        } else {
          this.getView().getModel("newUser").setProperty("/passwordState", "Error");
        }
      },

      checkConfirmPassword: function () {
        let userCheck = this.getView().getModel("newUser").getData();
        let confirmPassword = userCheck.confirm_password;
        if (confirmPassword) {
          this.getView().getModel("newUser").setProperty("/confirmPasswordState", "None");
        } else {
          this.getView().getModel("newUser").setProperty("/confirmPasswordState", "Error");
        }
      },

      onRegister: function () {
        const oData = this.getView().getModel("newUser").oData;
        const firstName = oData.firstName;
        const lastName = oData.lastName;
        const phoneNumber = oData.phoneNumber;
        const email = oData.email;
        const password = oData.password;
        const city = oData.city;
        const confirm_password = oData.confirm_password;

        let oRouter = this.getOwnerComponent().getRouter();
        
        if (Validations.validateName(firstName) && Validations.validateName(lastName) && Validations.validatePhone(phoneNumber) && Validations.validateEmail(email) && Validations.validatePassword(password) && city && confirm_password) 
        {
            const data = JSON.parse(JSON.stringify(oData));
            data.password = this.encryptPassword(data.password);
            this.post("http://localhost:3000/register", data)
              .then((response) => {
                MessageToast.show(this.getI18nMessage("register_page_welcome_message"));
                let newUser = new JsonModel();
                this.getView().setModel(newUser, "newUser");
                oRouter.navTo("Login");
              })
              .catch((err) => {
                console.log(err);
                if (err.status === 401 || err.status === 400 || err.status === 409) {
                  sap.m.MessageBox.error(`${err.responseJSON.error}` + ".");
                }
              })
          this.getView().getModel("newUser").setProperty("/firstNameState", "None");
          this.getView().getModel("newUser").setProperty("/lastNameState", "None");
          this.getView().getModel("newUser").setProperty("/phoneNumberState", "None");
          this.getView().getModel("newUser").setProperty("/emailState", "None");
          this.getView().getModel("newUser").setProperty("/passwordState", "None");
          this.getView().getModel("newUser").setProperty("/cityState", "None");
          this.getView().getModel("newUser").setProperty("/confirmPasswordState", "None");
        } else {
          sap.m.MessageBox.error(this.getI18nMessage("HIGHLIGHTED_FIELDS"));
          if (!firstName || !Validations.validateName(firstName)) {
            this.getView().getModel("newUser").setProperty("/firstNameState", "Error");
          }
          if (!lastName || !Validations.validateName(lastName)) {
            this.getView().getModel("newUser").setProperty("/lastNameState", "Error");
          }
          if (!Validations.validatePhone(phoneNumber)) {
            this.getView().getModel("newUser").setProperty("/phoneNumberState", "Error");
          }
          if (!Validations.validateEmail(email)) {
            this.getView().getModel("newUser").setProperty("/emailState", "Error");
          }
          if (!password) {
            this.getView().getModel("newUser").setProperty("/passwordState", "Error");
          }
          if (!city) {
            this.getView().getModel("newUser").setProperty("/cityState", "Error");
          }
          if (!confirm_password) {
            this.getView().getModel("newUser").setProperty("/confirmPasswordState", "Error");
          }
        }
      },
    });
  }
)