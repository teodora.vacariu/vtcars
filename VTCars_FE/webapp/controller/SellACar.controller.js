sap.ui.define(
  [
    "VTCARS_PROJECT/controller/Base.controller",
    "sap/ui/model/json/JSONModel",
    "jquery.sap.global",
    "sap/m/MessageToast",
    "sap/m/MessageBox"
  ],
  (BaseController, JSONModel, jQuery, MessageToast, MessageBox) => {
    return BaseController.extend("VTCARS_PROJECT.controller.SellACar", {
      getRouter: function () {
        return this.getOwnerComponent().getRouter();
      },
      onInit() {
        const oRouter = this.getOwnerComponent().getRouter();
        oRouter.getRoute("SellACar").attachMatched(this.setupData, this);
      },
      setupData: function () {
        this.verifyToken();
        this.byId("yearF").setMaxDate(new Date());
        this.byId("yearF").setMinDate(new Date("2000"));
        this.setupModels();
        this.setupUploadInput();
      },
      setupModels: function() {
        let modelFilter = new JSONModel();
        this.getView().setModel(modelFilter, "modelFilter");
        const id = this.getUserId();
        let newCar = new JSONModel({
          images: [],
          documents: [],
          owner: id,
        });
        this.getView().setModel(newCar, "newCar");
        this.get("http://localhost:3000/getBrandModel")
          .then((dataBrand) => {
            let brandM = new JSONModel(dataBrand);
            this.getView().setModel(brandM, "brandM");
          })
          .catch((err) => {
            console.log(err)
          })
      },
      setupUploadInput: function() {
        let comboSelectionCheck = new JSONModel();
        this.getView().setModel(comboSelectionCheck, "comboSelectionCheck");
        this.getView().getModel("comboSelectionCheck").setProperty("/isUploadEnabled", false);
        let oData = {
          SelectedAuth: "CarAuth",
          AuthCollection: [
            { StatusId: "Yes", Status: "Yes" },
            { StatusId: "No", Status: "No" },
          ],
          Editable: true,
          Enabled: true,
        };
        let oModel = new JSONModel(oData);
        this.getView().setModel(oModel, "SelectedModel");
      },
      onCancelPress: function (evt) {
        let newCar = new JSONModel();
        this.getView().setModel(newCar, "newCar");
        let oRouter = this.getOwnerComponent().getRouter();
        oRouter.navTo("LandingPage");
      },
      validateNumber: function (oValue) {
        let numberCheck = /^[0-9]*$/;
        return numberCheck.test(oValue);
      },
      checkMileage: function () {
        let carCheck = this.getView().getModel("newCar").getData();
        let mileage = carCheck.mileage;
        if (this.validateNumber(mileage)) {
          this.getView().getModel("newCar").setProperty("/mileageState", "None");
        } else {
          this.getView().getModel("newCar").setProperty("/mileageState", "Error");
        }
      },
      checkPrice: function () {
        let carCheck = this.getView().getModel("newCar").getData();
        let price = carCheck.price;
        if (this.validateNumber(price)) {
          this.getView().getModel("newCar").setProperty("/priceState", "None");
        } else {
          this.getView().getModel("newCar").setProperty("/priceState", "Error");
        }
      },
      
      checkTitle: function () {
        let carCheck = this.getView().getModel("newCar").getData();
        let title = carCheck.title;
        if (title) {
          this.getView().getModel("newCar").setProperty("/titleState", "None");
        } else {
          this.getView().getModel("newCar").setProperty("/titleState", "Error");
        }
      },

      onSelectionBrand(oEvent) {
        let selectedBrand = oEvent.getSource().getSelectedItem().getBindingContext("brandM").getObject().models;
        this.getView().getModel("modelFilter").setData(selectedBrand);
        let brandKey = this.getView().byId("comboBrandLanding").getSelectedKey();
        this.getOwnerComponent().getModel("filteredBy").setProperty("/brand", brandKey);
      },
      onSelectionChangeYes(oEvent) {
        let selectedOptions = oEvent.getSource().getSelectedKey();
        if (selectedOptions == "yes") {
          this.getView().getModel("comboSelectionCheck").setProperty("/isUploadEnabled", true);
        } else {
          this.getView().getModel("comboSelectionCheck").setProperty("/isUploadEnabled", false);
        }
      },
      handleValueChange: function (oEvent) {
        const fileObj = oEvent.getParameter("files");   //array cu toate obiectele fisierele  
        const keys = Object.keys(fileObj);              
        this.aSelectedImages = [];
        for (let i = 0; i < keys.length; i++) {
          this.aSelectedImages.push(fileObj[keys[i]]);
        }
        sap.m.MessageBox.information(this.getI18nMessage("PRESS_UPLOAD"));
      },
      handleTypeMissmatch: function (oEvent) {
        var aFileTypes = oEvent.getSource().getFileType();
        aFileTypes.map(function (sType) {
          return "*." + sType;
        });
        sap.m.MessageBox.error(
          this.getI18nMessage("THE_FILE_TYPE") +
          oEvent.getParameter("fileType") + " " +
          this.getI18nMessage("SUPPORTED_FILES") +
          aFileTypes.join(", ") + "."
        );
      },
      handleUploadPress: async function () {
        let formData = new FormData();
        for (let i = 0; i < this.aSelectedImages.length; i++) {
          formData.append("files[]", this.aSelectedImages[i]);
        }
        const data = await jQuery.ajax({
          type: "POST",
          url: "http://localhost:3000/images",
          cache: false,
          contentType: false,
          processData: false,
          enctype: "multipart/form-data",
          data: formData,
          xhrFields: {
            withCredentials: true,
          },
          success: (data) => {
            MessageToast.show(this.getI18nMessage("UPLOAD_SUCCESSFUL"));
            return data;
          },
          error: (err) => {
            MessageToast.show(err);
          },
        });
        this.getView().getModel("newCar").setProperty("/images", data.map((item) => item.id));
      },
      handleValueChangeDoc: function (oEvent) {
        const fileObj = oEvent.getParameter("files");
        const keys = Object.keys(fileObj);
        this.aSelectedDocuments = [];
        for (let i = 0; i < keys.length; i++) {
          this.aSelectedDocuments.push(fileObj[keys[i]]);
        }
        sap.m.MessageBox.information(this.getI18nMessage("PRESS_UPLOAD"));
      },
      handleTypeMissmatchDoc: function (oEvent) {
        var aFileTypes = oEvent.getSource().getFileType();
        aFileTypes.map(function (sType) {
          return "*." + sType;
        });
        sap.m.MessageBox.error(
          this.getI18nMessage("THE_FILE_TYPE") +
          oEvent.getParameter("fileType") + " " +
          this.getI18nMessage("SUPPORTED_FILES") +
          aFileTypes.join(", ") + "."
        );
      },
      handleUploadPressDoc: async function (oEvent) {
        let formData = new FormData();
        for (let i = 0; i < this.aSelectedDocuments.length; i++) {
          formData.append("docs[]", this.aSelectedDocuments[i]);
        }
        const data = await jQuery.ajax({
          type: "POST",
          url: "http://localhost:3000/documents",
          cache: false,
          contentType: false,
          processData: false,
          enctype: "multipart/form-data",
          data: formData,
          xhrFields: {
            withCredentials: true,
          },
          success: (data) => {
            MessageToast.show(this.getI18nMessage("UPLOAD_SUCCESSFUL"));
            return data;
          },
          error: (err) => {
            MessageToast.show(err);
          },
        });
        this.getView().getModel("newCar").setProperty("/documents", data.map((item) => item.id));
      },
      onApplyPress: function () {
        const oData = this.getView().getModel("newCar").oData;
        const mileage = oData.mileage;
        const price = oData.price;
        const title = oData.title;
        if (this.validateNumber(mileage) && this.validateNumber(price) && title) {
          if (!this.dDialog) {
            this.dDialog = this.loadFragment({
              name: "VTCARS_PROJECT.view.SellACar",
            });
          }
          this.dDialog.then(function (oDialog) {
            oDialog.open();
          });
          this.getView().getModel("newCar").setProperty("/titleState", "None");
          this.getView().getModel("newCar").setProperty("/mileageState", "None");
          this.getView().getModel("newCar").setProperty("/priceState", "None");
        }
        else {
          sap.m.MessageBox.error(this.getI18nMessage("HIGHLIGHTED_FIELDS"));
          if (!this.validateNumber(mileage) || !mileage) {
            this.getView().getModel("newCar").setProperty("/mileageState", "Error");
          }
          if (!this.validateNumber(price) || !price) {
            this.getView().getModel("newCar").setProperty("/priceState", "Error");
          }
          if (!price) {
            this.getView().getModel("newCar").setProperty("/titleState", "Error");
          }
        }
      },
      onCancelAddDialog: function () {
        this.byId("SellACarDialog").close();
      },
      onAddAccepted: async function () {
        if (this.getView().getModel("newCar").oData.isVerified === "Yes") {
          this.getView().getModel("newCar").setProperty("/isVerified", true);
        } else {
          this.getView().getModel("newCar").setProperty("/isVerified", false);
        }
        const response = this.post("http://localhost:3000/car", this.getView().getModel("newCar").oData)
          .then((data) => {
            MessageToast.show(this.getI18nMessage("ADDED_SUCCESSFULLY"));
            let newCar = new JSONModel();
            this.getView().setModel(newCar, "newCar");
            let oRouter = this.getOwnerComponent().getRouter();
            oRouter.navTo("LandingPage");
            return data;
          })
          .catch((err) => {
            sap.m.MessageBox.error(this.getI18nMessage("MISSING_INFORMATION"));
             this.byId("SellACarDialog").close();
             console.log("Post err", err);
             return false;
          })
      },
    });
  }
)