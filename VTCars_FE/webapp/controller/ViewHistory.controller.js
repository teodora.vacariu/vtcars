sap.ui.define(
  [
    "VTCARS_PROJECT/controller/Base.controller",
    "jquery.sap.global",
    "sap/ui/core/Fragment",
    "sap/m/MessageToast"
  ],
  (BaseController, jQuery, Fragment, MessageToast) => {
    return BaseController.extend("VTCARS_PROJECT.controller.ViewHistory", {
      onInit() {
        const oRouter = this.getOwnerComponent().getRouter();
        oRouter
          .getRoute("ViewHistory")
          .attachMatched(this.historyPageFunction, this);
        
      },

      historyPageFunction: function () {
        this.verifyToken();
        this.getHistory();
        this.showBusyIndicator(2000, 0);
      },

      getHistory: async function () {
        let resArray = [];
        const userId = this.getUserId();

        const data = await this.get("http://localhost:3000/user/" + userId);
        if (data.listedCars.length > 0) {
          data.listedCars.forEach(async (element) => {
            let listLength = data.listedCars.length;
            const response = await this.get(
              "http://localhost:3000/car/" + element
            );
            if (response) {
              resArray.push(response);
              if (listLength === resArray.length) {
                this.getOwnerComponent()
                  .getModel("listedPosts")
                  .setData(resArray);
              }
            } else {
              console.log("err");
            }
          });
        } else {
          this.getOwnerComponent().getModel("listedPosts").setData({});
        }
      },

      onPressDeletePost: function (oEvent) {
        let selectedCar = oEvent
          .getSource()
          .getBindingContext("listedPosts")
          .getObject();
        this.getView().getModel("selectedCar").setData(selectedCar);
        let oView = this.getView();
        if (!this.byId("deleteDialog")) {
          Fragment.load({
            id: oView.getId(),
            name: "VTCARS_PROJECT.view.DeletePost",
            controller: this,
          }).then(function (oDialog) {
            oView.addDependent(oDialog);
            oDialog.open();
          });
        } else {
          this.byId("deleteDialog").open();
        }
      },

      handleYesDelete: function () {
        let id = this.getView().getModel("selectedCar").getData()._id;
        this.delete("http://localhost:3000/car/" + id)
          .then((response) => {
            this.getHistory();
            this.byId("deleteDialog").close();
            MessageToast.show(this.getI18nMessage("DELETE_POST"))
          })
          .catch((err) => {
            console.log(err);
          });
      },

      handleCancelDelete: function () {
        this.byId("deleteDialog").close();
      },
    });
  }
);
