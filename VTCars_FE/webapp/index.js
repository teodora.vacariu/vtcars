sap.ui.define(
  ["sap/ui/core/ComponentContainer"],
  function (ComponentContainer) {
    "use strict";

    new ComponentContainer({
      name: "VTCARS_PROJECT",
      settings: {
        id: "vtcars",
      },
      async: true,
    }).placeAt("content");
  }
);
